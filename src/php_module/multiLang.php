<?php 
/**
 * Class for multi language integration.
 * Need json files with language (Ex: fr.json , en.json)
 */
class multiLang {
    private $_lang;
    private $_rep;
    private $_page;
	private $_ramUsage; // 1 = true | 0 = false
    private $_contentFileRAM;
    private $_fullFilePath;

    /**
     * IN : {'lang' => language, 'rep' => traduction directory, 'page' => page name, 'ramUsage' => boolean use ram for save  traduction file (default false) }
     * REP : example : ./trad -> traduction file : <rep>/<page>_<lang>.json (./trad/index_en.json)
     */
    public function __construct($param){
        $this->_lang = $this->checkEntry($param, 'lang', 'en');
        $this->_rep = $this->checkEntry($param, 'rep', './files');
        $this->_page = $this->checkEntry($param, 'page', '');
        $this->_ramUsage = $this->checkEntry($param, 'ramUsage', '0');
        if($this->_page == ''){
            $this->_fullFilePath = $this->_rep.'/'.$this->_lang.'.json';
        }
        else {
            $this->_fullFilePath = $this->_rep.'/'.$this->_page.'_'.$this->_lang.'.json';
        }
    }

    /**
     * return : array[ 'value' , 'status' ]
     * Status : {
     *              200 : Ok                        -> Value : <Value>
     *              204 : No Content                -> Value : ''
     *              404 : Not Found                 -> Value : NULL
     *              500 : Internal Server Error     -> Value : NULL
     *          }
     */
    function get($id){
        return $this->getById($id);
    }

    /**
     * Chack if language exist
     * Return : 0 Not Exist | 1 Exist 
     */
    function checkLang(){
        if(file_exists($this->_fullFilePath)){ return '1'; } else{ return '0'; }
    } 
    
	/**
	 * Unset variable traduction from file
	 */
	function unsetRAM(){
		$this->_contentFileRAM = '';
	}
	
	/**
	 * Get RAM data
	 * Return : string (contain all data JSON from traduction file)
	 */
	function getRAM(){
		return $this->_contentFileRAM;
	}

    /**
     * Get current language
     */
    function getLang(){
        return $this->_lang;
    }

    /**
     * Get current page
     */
    function getPage(){
        return $this->_page;
    }

    /**
     * Set page
     * Function disabled : Not useful
     */
    /*function setPage($page)
    {
        $param['page'] = $page;
        $this->_page = $this->checkEntry($param, 'page', 'index');
		$this->unsetRAM();
    }*/

    /**
     * Get traduction directory
     */
    function getLangRep(){
        return $this->_rep;
    }

    /**
     * IN : ID
     * Return JSON if exist
     */
    private function getById($id){
        try {
			if($this->_ramUsage == '1')
			{
				if($this->_contentFileRAM != ''){ $jsonLang = $this->_contentFileRAM; }
				else{
					if($this->checkLang() == 0){ return $this->createAnswer(NULL, '404'); }
					$this->_contentFileRAM = file_get_contents($this->_fullFilePath);
					$jsonLang = $this->_contentFileRAM;
				}
			}
			else{
				if($this->checkLang() == 0){ return $this->createAnswer(NULL, '404'); }
				$jsonLang = file_get_contents($this->_fullFilePath);
			}
            $arrayJson = json_decode($jsonLang, true);
            if(!isset($arrayJson[$id])){
                return $this->createAnswer('', '404');
            }
            if($arrayJson[$id] == ''){
                return $this->createAnswer('', '204');
            }
            return $this->createAnswer($arrayJson[$id], '200');
        } catch (Exception $e) {
            return $this->createAnswer(NULL, '500');
        }
    }

    /**
     * Check if object[<>] is initialized
     */
    private function checkEntry($param, $id , $defaultValue){
        if(isset($param[$id])){ return $param[$id];}
        else{ return $defaultValue; }
    }

    /**
     * Create JSON message
     */
    private function createAnswer($value, $status){
        $temp = array('value' => $value, 'status' => $status);
        return $temp;
    }
}