<?php
require '../php_module/multiLang.php';
// Just for testing

$param = array(
    'lang' => 'en',
    'page' => 'index',
    'rep' => '../trad'
);
$en = new multiLang($param);
$result = $en->get('test');
echo('<h1>Result status : 200</h1>');
echo json_encode($result);
echo '<br>';
echo '$result["value"]["ex"]  :  '.$result['value']['ex']; //Example multiple result
echo '<br><br>';





$en2 = array(
    'lang' => 'en',
    'rep' => '../trad',
    'page' => 'index'
);
$simple = new multiLang($param);
echo json_encode($simple->get('test'));





echo('<h1>Result status : 204</h1>');
$error204 = new multiLang($en2);
echo json_encode($error204->get('empty'));





echo '<br><br>';
echo('<h1>Result status : 404</h1>');
$error404 = new multiLang($en2);
echo json_encode($error404->get('hello'));




echo '<br><br>';
echo('<h1>Test on second "page"</h1>');
$en3 = array(
    'lang' => 'en',
    'rep' => '../trad',
    'page' => 'about'
);
$sencondPage = new multiLang($en3);
echo json_encode($sencondPage->get('title'));




echo '<br><br>';
echo('<h1>FR - "PROD" Test</h1>');
$en4 = array(
    'lang' => 'fr',
    'rep' => '../trad',
    'page' => 'index'
);
$prodTest = new multiLang($en4);
echo json_encode($prodTest->get('contact')['value']['form-container_subject']['subject']);




echo '<br><br>';
echo('<h1>FR - Data in RAM</h1>');
$en5 = array(
    'lang' => 'fr',
    'rep' => '../trad',
    'page' => 'index',
	'ramUsage' => '1'
);
$useRam = new multiLang($en5);
echo json_encode($useRam->get('contact')['value']['form-container_subject']['subject']);
try{
	echo '<br>Before value : <br>'.$useRam->getRAM();
	$useRam->unsetRAM();
	echo '<br>After value : <br>'.$useRam->getRAM();
	echo '<br>OK - unsetTradData()<br>';
} catch (Exception $e) {
	echo '<br>FAIL - unsetTradData()<br>';
}
echo json_encode($useRam->get('contact')['value']['form-container_subject']['optional']);
try{
	echo '<br>Before value : <br>'.$useRam->getRAM();
	$useRam->unsetRAM();
	echo '<br>After value : <br>'.$useRam->getRAM();
	echo '<br>OK - unsetTradData()<br>';
} catch (Exception $e) {
	echo '<br>FAIL - unsetTradData()<br>';
}

echo '<br><br>';
echo('<h1>FR - No page name</h1>');
$en6 = array(
    'lang' => 'fr',
    'rep' => './trad',
    'page' => '',
	'ramUsage' => '1'
);
$noPage = new multiLang($en6);
echo json_encode($noPage->get('title'));