// © 2019 - neptium.ch
// Require : JQuery 3.3.1 or +
var configFile = "../js_module/mLang_conf.json";

class Logs {
  constructor(mode) {
    this.mode = mode = undefined ? "prod" : mode;
  }
  setMode(mode) {
    this.mode = mode = undefined ? "prod" : mode;
  }
  info(content) {
    if (this.mode == "dev")
      console.info(`Multilingualism module info  : ${content}`);
  }
  error(content) {
    console.error(`Multilingualism module error : ${content}`);
  }
  debug(content) {
    if (this.mode == "dev")
      console.debug(`Multilingualism module debug : ${content}`);
  }
}

let logs = new Logs();

$.get(configFile, (data) => {
  var tradFile = `${data["files_folder"]}/${$("html").attr("page")}_${
    document.documentElement.lang
    }.json`;
  var attribute_name = data["attribute_name"];
  var removeAttr = data["remove_tag"];
  logs.setMode(data["logs_mode"]);
  $.get(tradFile, (data) => {
    $(`[${attribute_name}]`).each(() => {
      var commands = $(this)
        .attr(attribute_name)
        .split(";");
      for (var id in commands) {
        var request = commands[id];
        if (request.split("?")[1] == null) {
          $(this).text(getValue(request, data));
        } else {
          $(this).attr(request.split("?")[1], getValue(request, data));
        }
        if (removeAttr)
          $(this).removeAttr(attribute_name);
      }
    });
  }).fail(() => {
    logs.error(`Translation file not found (${tradFile})`);
  });
}).fail(() => {
  console.error("Multilingualism module error : Config file not found");
});

function getValue(request, data) {
  try {
    var requestWithoutParam = request.split("?")[0];
    var split_character = "|";
    requestWithoutParam = requestWithoutParam.split(split_character);
    for (var id in requestWithoutParam) {
      data = data[requestWithoutParam[id]];
    }
    if (data == undefined) {
      logs.info(`Cannot find translation of "${request}"`);
      return "";
    }
    return data;
  } catch (error) {
    logs.error(error);
    return "";
  }
}
