# Short description 
This microframework allows the implementation of multilingualism in a website. On the server (PHP Module) or client (JS Module) side

## Wiki 
- [PHP Module](https://gitlab.com/Neptium/multilang/wikis/PHP-Module)
- [JS Module](https://gitlab.com/Neptium/multilang/wikis/JS-Module)

## Concept
### Operating diagram JS Module
![shema](https://gitlab.com/Neptium/multilang/raw/master/documentation/PrincipleOfOperationJS.PNG)

### Operating diagram PHP module
![shema](https://gitlab.com/Neptium/multilang/raw/master/documentation/PrincipleOfOperationPHP.PNG)

